CC = gcc
OBJECTS = relay-8way.o
LIBS = -lftdi
CFLAGS = -Wall -O2 -g
NAME = relay-8way
BINDIR = $(DESTDIR)/usr/bin

cbg: $(OBJECTS)
	$(CC) -o $(NAME) $(OBJECTS) $(LIBS)

%.o: %.c
	$(CC) -c $(CFLAGS) $<

clean:
	rm *.o $(NAME)

install:
	install --mode=777 $(NAME) $(BINDIR)/

uninstall:
	rm $(BINDIR)/$(NAME)
